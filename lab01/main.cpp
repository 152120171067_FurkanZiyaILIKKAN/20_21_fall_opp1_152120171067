#include <iostream>
#include <fstream>
int toplama(int n, int nums[]) {
    int toplam = 0;
    for (int i = 0; i < n; i++)
        toplam += nums[i];

    return toplam;
}
int carpma(int n, int nums[]) {
    int carpim = 1;
    for (int i = 0; i < n; i++)
        carpim = nums[i];

    return carpim;
}
int ortalama(int n, int nums[]) {
    return toplama(n, nums) / n;
}
int minbulma(int n, int nums[]) {
    int min = nums[0];
    for (int i = 1; i < n; i++)
        if (min > nums[i])
            min = nums[i];
    return min;
}

using namespace std;

int main() {
    int count;
    string dosyaismi;
    ifstream dosya;
    cout << "Please enter name of the text file: ";
    cin >> dosyaismi;
    dosya.open(dosyaismi, ios::in);
    if (!dosya) {
        cout << "Txt File Not Opened!!! Please check the name of the file. " << endl;
        exit(1); // terminate with error
    }

    dosya >> count;

    int numbers = new int[count];
    for (int i = 0; i < count; i++) {
        dosya >> numbers[i];
    }
    dosya.close();



    cout << "Sum: " << toplama(count, numbers) << endl;
    cout << "Product: " << carpma(count, numbers) << endl;
    cout << "Average: " << ortalama(count, numbers) << endl;
    cout << "Minumum Value: " << minbulma(count, numbers) << endl;



    return 0;
    system("PAUSE");
}