#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int N, num;
    cin >> N;
    vector<int> nums;
    for (int i = 0; i < N; i++) {
        cin >> num;
        nums.push_back(num);
    }
    for (int i = N - 1; i >= 0; i--)
        cout << nums[i] << " ";
    return 0;
}