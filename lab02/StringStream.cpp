#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    int count = 0;
    for (int i = 0; i < str.length(); i++)
        if (str[i] == ',')
            count++;

    stringstream x(str);
    vector<int> a;
    char buffer;
    int temp;
    for (int i = 0; i < count + 1; i++) {
        if (i < count) {
            x >> temp;

            a.push_back(temp);
            x >> buffer;
        }
        else {
            x >> temp;
            a.push_back(temp);
        }

    }
    return a;

}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}